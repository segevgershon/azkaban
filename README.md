# Azkaban

Keeps my aliases, rcs and configs, so it can be cloned whenever I settle in a 
new environment quickly.


## Manual Requirements

For better nvim experience download a Nerdfont font and set it as the font of
your terminal (I like JetBrainesMono).
You can find all the nerd font [here](https://github.com/ryanoasis/nerd-fonts).
