#!/bin/bash

# Install apt requirements
echo [+] Installing requirements
sudo apt install < apt-requirements.txt
# Ripgrep and bat aren't friends
sudo apt install -o Dpkg::Options::="--force-overwrite" ripgrep

echo [+] Creating home
rm -f $HOME/.gitconfig \
      $HOME/.aliases \
      $HOME/.bashrc \
      $HOME/.bash_aliases \
      $HOME/.bash_profile \
      $HOME/.zshrc \
      $HOME/.config/nvim/init.vim

ln -s $PWD/sh/aliases $HOME/.aliases

# Copy bash configs
ln -s $PWD/bash/bashrc       $HOME/.bashrc
ln -s $PWD/bash/bash_aliases $HOME/.bash_aliases

# Zsh setup
sh $PWD/zsh/install

# Fzf setup
sh $PWD/fzf/install

ln -s $PWD/zsh/zshrc $HOME/.zshrc
ln -s $PWD/zsh/p10k.zsh $HOME/.p10k.zsh

# Copy git config
ln -s $PWD/git/gitconfig     $HOME/.gitconfig

# Copy nvim setup
mkdir -p $HOME/.config/nvim
ln -s $(find $PWD/nvim/ -mindepth 1 -maxdepth 1 -print) $HOME/.config/nvim/

# Create folder structure
mkdir $HOME/repos
mkdir $HOME/sandbox

zsh
